package org.launchcode.launchcart;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.launchcart.data.CustomerRepository;
import org.launchcode.launchcart.data.ItemRepository;
import org.launchcode.launchcart.models.Cart;
import org.launchcode.launchcart.models.Customer;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class CustomerRestControllerTest extends AbstractBaseRestIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Before
    public void createTestCustomers() {
        customerRepository.save(new Customer("test1", "test1"));
        customerRepository.save(new Customer("test2", "test2"));

        Item item1 = new Item("Test Item 1", 1, "", false);
        itemRepository.save(item1);
        Item item2 = new Item("Test Item 2", 2, "", false);
        itemRepository.save(item2);
        Item item3 = new Item("Test Item 3", 3, "", false);
        itemRepository.save(item3);

        Cart cart = customerRepository.findByUsername("test1").getCart();
        cart.addItem(item1);
        cart.addItem(item2);
        Cart cart2 = customerRepository.findByUsername("test2").getCart();
        cart2.addItem(item3);
    }

    @Test
    public void getAllCustomers() throws Exception {
        List<Customer> customers = customerRepository.findAll();
        ResultActions res = mockMvc.perform(get("/api/customers"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(customers.size())));

        for (int i = 0; i < customers.size(); i++) {
            res.andExpect(jsonPath("$[" + i + "].uid", is(customers.get(i).getUid())));
        }
    }

    @Test
    public void getCustomerById() throws Exception {
        Customer customer = customerRepository.findAll().get(0);
        mockMvc.perform(get("/api/customers/{id}", customer.getUid()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("uid").value(customer.getUid()));
    }

    @Test
    public void getCustomerByIdNotFound() throws Exception {
        mockMvc.perform(get("/api/customers/-1"))
            .andExpect(status().isNotFound());
    }

    @Test
    public void getCustomerCart() throws Exception {
        Customer customer = customerRepository.findByUsername("test1");

        ResultActions res = mockMvc.perform(get("/api/customers/{id}/cart", customer.getUid()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("uid").value(customer.getCart().getUid()));
    }

    @Test
    public void getCustomerCartNotFound() throws Exception {
        ResultActions res = mockMvc.perform(get("/api/customers/-1/cart"))
            .andExpect(status().isNotFound());
    }
}

package org.launchcode.launchcart.models;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Created by LaunchCode
 */
public class TestCart {

    private Cart cart;

    @Before
    public void setupCart() {
        cart = new Cart();
    }

    @Test
    public void testAddItem() {
        Item item = new Item("Test Item", 5);
        assertFalse(cart.getItems().contains(item));
        cart.addItem(item);
        assertTrue(cart.getItems().contains(item));
    }

    @Test
    public void testRemoveItem() {
        Item item = new Item("Test Item", 5);
        cart.addItem(item);
        assertTrue(cart.getItems().contains(item));
        cart.removeItem(item);
        assertFalse(cart.getItems().contains(item));
    }

    @Test
    public void testComputeTotal () {
        cart.addItem(new Item("Test Item 1", 5));
        cart.addItem(new Item("Test Item 1", 7.2));
        cart.addItem(new Item("Test Item 1", 0.3));
        assertEquals(cart.computeTotal(), 12.5, 0.001);
    }

    @Test
    public void removeAllItems(){
        cart.addItem(new Item("Test Item 1", 5));
        cart.addItem(new Item("Test Item 1", 7.2));
        cart.addItem(new Item("Test Item 1", 0.3));

        cart.removeAllItems();

        assertEquals(0, cart.getItems().size(), 0.0);
    }

    @Test
    public void addMultipleItems(){
        List<Item> items = new ArrayList<>();

        items.add(new Item("Test Item 1", 5));
        items.add(new Item("Test Item 1", 7.2));
        items.add(new Item("Test Item 1", 0.3));

        cart.removeAllItems();
        cart.addItems(items);

        assertEquals(3, cart.getItems().size(), 0.0);
    }
}

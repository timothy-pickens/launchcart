package org.launchcode.launchcart;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.launchcart.data.CartRepository;
import org.launchcode.launchcart.data.CustomerRepository;
import org.launchcode.launchcart.data.ItemRepository;
import org.launchcode.launchcart.models.Cart;
import org.launchcode.launchcart.models.Customer;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class CartRestControllerTest extends AbstractBaseRestIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private CartRepository cartRepository;

    @Before
    public void setUpTestRepositories() {
        customerRepository.save(new Customer("test1", "test1"));
        customerRepository.save(new Customer("test2", "test2"));

        Item item1 = new Item("Test Item 1", 1, "", false);
        itemRepository.save(item1);
        Item item2 = new Item("Test Item 2", 2, "", false);
        itemRepository.save(item2);
        Item item3 = new Item("Test Item 3", 3, "", false);
        itemRepository.save(item3);

        Cart cart = customerRepository.findByUsername("test1").getCart();
        cart.addItem(item1);
        cart.addItem(item2);
        Cart cart2 = customerRepository.findByUsername("test2").getCart();
        cart2.addItem(item3);

        cartRepository.save(cart);
        cartRepository.save(cart2);
    }

    @Test
    public void getCarts() throws Exception {
        List<Cart> carts = cartRepository.findAll();

        ResultActions res = mockMvc.perform(get("/api/carts"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("$", hasSize(carts.size())));

        for (int i = 0; i < carts.size(); i++) {
            res.andExpect(jsonPath("$[" + i + "].uid", is(carts.get(i).getUid())));
        }
    }

    @Test
    public void getCartById() throws Exception {
        Cart cart = cartRepository.findAll().get(0);
        mockMvc.perform(get("/api/carts/{id}", cart.getUid()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(contentType))
            .andExpect(jsonPath("uid").value(cart.getUid()));
    }

    @Test
    public void getCartByIdNotFound() throws Exception {
        mockMvc.perform(get("/api/carts/-1"))
            .andExpect(status().isNotFound());
    }

    @Test
    public void putCartAddItem() throws Exception {
        Cart cart = cartRepository.findAll().get(0);
        Item item = new Item("Item to Add", 17);
        int cartSize = cart.getItems().size();
        itemRepository.save(item);

        cart.addItem(item);

        String json = json(cart);

        cart.removeItem(item);

        mockMvc.perform(put("/api/carts/{id}", cart.getUid())
            .content(json).contentType(contentType))
            .andExpect(status().isOk());

        assertEquals((cartSize + 1), cartRepository.findOne(cart.getUid()).getItems().size(), 0.0);
    }

    @Test
    public void putCartRemoveItem() throws Exception {
        Cart cart = cartRepository.findAll().get(0);
        Item item = cart.getItems().get(0);
        int cartSize = cart.getItems().size();

        cart.removeItem(item);

        String json = json(cart);

        cart.addItem(item);

        mockMvc.perform(put("/api/carts/{id}", cart.getUid())
            .content(json).contentType(contentType))
            .andExpect(status().isOk());

        assertEquals(cartSize - 1, cartRepository.findOne(cart.getUid()).getItems().size(), 0.0);
    }

    @Test
    public void putCartNotFound() throws Exception {
        Cart cart = cartRepository.findAll().get(0);
        Item item = cart.getItems().get(0);
        int cartSize = cart.getItems().size();

        cart.removeItem(item);

        String json = json(cart);

        mockMvc.perform(put("/api/carts/-1")
            .content(json).contentType(contentType))
            .andExpect(status().isNotFound());
    }

    @Test
    public void putCartIdNotEqualToCartParam() throws Exception {
        Cart cart = cartRepository.findAll().get(0);
        String json = json(cart);

        mockMvc.perform(put("/api/carts/{id}", cartRepository.findAll().get(1).getUid())
            .content(json).contentType(contentType))
            .andExpect(status().isConflict());
    }
}

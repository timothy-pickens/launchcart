package org.launchcode.launchcart.data;

import org.launchcode.launchcart.models.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends JpaRepository<Item, Integer> {
    List<Item> findByPrice(Double price);

    List<Item> findByNewItem(boolean newItem);

    List<Item> findByPriceAndNewItem(Double price, boolean newItem);
}

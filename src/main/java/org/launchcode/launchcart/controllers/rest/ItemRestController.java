package org.launchcode.launchcart.controllers.rest;

import java.util.List;
import java.util.Optional;
import org.launchcode.launchcart.data.ItemRepository;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/items")
public class ItemRestController {

    @Autowired
    ItemRepository itemRepository;

    @GetMapping("")
    public List<Item> getItems(@RequestParam Optional<Double> price,
        @RequestParam("new") Optional<Boolean> newItem) {
        if (price.isPresent() && newItem.isPresent()) {
            return itemRepository.findByPriceAndNewItem(price.get(), newItem.get().booleanValue());
        }
        if (price.isPresent()) {
            return itemRepository.findByPrice(price.get());
        }
        if (newItem.isPresent()) {
            return itemRepository.findByNewItem(newItem.get().booleanValue());

        }
        return itemRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity getItem(@PathVariable int id) {
        Item item = itemRepository.findOne(id);

        if (item == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(item, HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity createItem(@RequestBody Item item) {
        return new ResponseEntity(itemRepository.save(item), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity updateItem(@PathVariable int id, @RequestBody Item newItem) {
        if (itemRepository.findOne(id) == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        if (id != newItem.getUid()) {
            return new ResponseEntity(HttpStatus.CONFLICT);
        }
        return new ResponseEntity(itemRepository.save(newItem), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity deleteItem(@PathVariable int id) {
        if (itemRepository.findOne(id) == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        itemRepository.delete(id);

        return new ResponseEntity(HttpStatus.OK);
    }
}

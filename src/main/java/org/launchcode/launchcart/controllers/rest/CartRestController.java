package org.launchcode.launchcart.controllers.rest;

import org.launchcode.launchcart.data.CartRepository;
import org.launchcode.launchcart.models.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/carts")
public class CartRestController {

    @Autowired
    CartRepository cartRepository;

    @GetMapping("")
    public ResponseEntity getCarts() {
        return new ResponseEntity(cartRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity getCart(@PathVariable int id) {
        Cart cart = cartRepository.findOne(id);

        if (cart == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(cart, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity updateCart(@PathVariable int id, @RequestBody Cart updatedCart) {
        Cart cart = cartRepository.findOne(id);

        if (cart == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        if (id != updatedCart.getUid()) {
            return new ResponseEntity(HttpStatus.CONFLICT);
        }

        cartRepository.save(updatedCart);

        return new ResponseEntity(HttpStatus.OK);
    }
}
